from flask import Flask
from flask import render_template, flash, redirect, url_for, request, jsonify

from flask.ext.login import LoginManager, UserMixin, login_required, login_user, logout_user, current_user

from forms import LoginForm
from forms import RegisterForm

from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)

import os

import arrow  # Replacement for datetime, based on moment.js

from pymongo import MongoClient
import pymongo

import pandas

from config import Config
import password
import acp_times_frontend

app = Flask(__name__)
app.config.from_object(Config)

client = MongoClient('mongodb+srv://dbUser:47D5rsmgP9MjDGO2@brevets-eusqj.mongodb.net/test?retryWrites=true&w=majority')
db = client.controls
users = client.users

login_manager = LoginManager()
login_manager.init_app(app)

@app.route('/')
@app.route('/index')
@login_required
def index():
    _items = db.controls.find()
    items = [item for item in _items]

    return render_template('calculator.html', items=items)

@app.route('/results')
@login_required
def results():
    _items = db.controls.find()
    items = [item for item in _items]
    app.logger.info(len(items))
    if len(items) == 0:
        return render_template('empty.html')
    else:
        return render_template('results.html', items=items)

@app.route('/new', methods=['POST'])
@login_required
def new():
    if request.form['km'] != '':
        startdatetime = arrow.get(str(request.form['begin_date']) + 'T' + str(request.form['begin_time']) + "-08:00")
        item_doc = {
            'distance': request.form['distance'],
            'location': request.form['km'],
            'locationname': request.form['location'],
            'open': acp_times_frontend.open_time(float(request.form['km']), float(request.form['distance']), startdatetime),
            'close': acp_times_frontend.open_time(float(request.form['km']), float(request.form['distance']), startdatetime)
        }
        db.controls.insert_one(item_doc)

    return redirect(url_for('controls'))

@app.route("/_calc_times")
@login_required
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    km = request.args.get('km', type=float) # Removed restriction to allow for higher input
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    brevetdist = request.args.get('distance', type=float)

    startdatetime = arrow.get(request.args.get('begin_date', "2017-01-01", type=str) + 'T' + request.args.get('begin_time', "00:00", type=str) + "-08:00")

    open_time = acp_times_frontend.open_time(km, brevetdist, startdatetime)
    close_time = acp_times_frontend.close_time(km, brevetdist, startdatetime)
    result = {"open": open_time, "close": close_time}
    return jsonify(result=result)

@app.route("/listAll/", defaults={'type':'json'})
@app.route("/listAll/<path:type>")
@login_required
def all(type):

    top = request.args.get('top', default = -1, type = int)

    controls = db.controls.find().sort('location', pymongo.ASCENDING)
    openTime = popToNum(top, list(cursorToList("open", controls)))
    controls = db.controls.find().sort('location', pymongo.ASCENDING)
    closeTime = popToNum(top, list(cursorToList("close", controls)))
    if type == "csv":
        return {"open": openTime, "close": closeTime, "csv": 1}
    return jsonify({"open": openTime, "close": closeTime, "top": top})

@app.route("/listOpenOnly/", defaults={'type':'json'})
@app.route("/listOpenOnly/<path:type>")
@login_required
def openOnly(type):

    top = request.args.get('top', default = -1, type = int)

    controls = db.controls.find().sort('location', pymongo.ASCENDING)
    openTime = popToNum(top, list(cursorToList("open", controls)))
    if type == "csv":
        return {"open": openTime, "csv": 1}
    return jsonify({"open": openTime})

@app.route("/listCloseOnly/", defaults={'type':'json'})
@app.route("/listCloseOnly/<path:type>")
@login_required
def closeOnly(type):
    
    top = request.args.get('top', default = -1, type = int)

    controls = db.controls.find().sort('location', pymongo.ASCENDING)
    closeTime = popToNum(top, list(cursorToList("close", controls)))
    if type == "csv":
        return {"close": closeTime}
    return jsonify({"close": closeTime})

def cursorToList(index, csr):
    returnList = []
    for item in csr:
        returnList.append(item[index])

    return returnList
            
def popToNum(num, li):
    if num < 0:
        return li
    while len(li) > num:
        li.pop()
    return li

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = form.username.data
        myuser = users.users.find( { 'user': user } )
        hash = myuser[0]['password']
        if password.verify_password(form.password.data, hash):
            creds = User(myuser[0]['_id'])
            login_user(creds, remember=form.remember_me.data, force=True)
            return redirect(url_for('index'))
    return render_template('login.html',  title='Sign In', form=form)


@app.route('/api/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        item_doc = {
            'user': form.username.data,
            'password': password.hash_password(form.password.data)
            }
        users.users.insert_one(item_doc)
    return render_template('register.html',  title='Register', form=form)

@app.route('/api/token', methods=['GET', 'POST'])
@login_required
def generate_auth_token(expiration=600):
   s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
   # pass index of user
   return s.dumps({'id': current_user.id})

@login_manager.user_loader
def load_user(user_id):
    return User(user_id)

class User(UserMixin):

    def __init__(self, id):
        self.id = id

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
